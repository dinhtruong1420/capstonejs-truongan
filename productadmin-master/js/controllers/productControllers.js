function layThongTinTuForm() {
    var name = document.getElementById("name").value.trim();
    var price = document.getElementById("price").value.trim();
    var screen = document.getElementById("screen").value.trim();
    var backCam = document.getElementById("backCam").value.trim();
    var frontCam = document.getElementById("frontCam").value.trim();
    var img = document.getElementById("img").value.trim();
    var desc = document.getElementById("desc").value.trim();
    var type = document.getElementById("type").value.trim();

    var product = new Product(name, price, screen, backCam, frontCam, img, desc, type);
    console.log('product: ', product);
    return product;
}

function renderDsProduct(list) {
    var contentHTML = "";
    for (var i = 0; i < list.length; i++) {
      var currentProduct = list[i];
      var contentTr = `<tr> 
      <td>${currentProduct.id}</td>
      <td>${currentProduct.name}</td>
      <td>${currentProduct.price}</td>
      <td>${currentProduct.screen}</td>
      <td>${currentProduct.backCam}</td>
      <td>${currentProduct.frontCam}</td>
      <td><img class="img-fluid" src="${currentProduct.img}" /></td>
      <td>${currentProduct.desc}</td>
      <td>${currentProduct.type}</td>
      <td>
      <button onclick="deleteProduct(${currentProduct.id})"  class="btn btn-danger"><i class="fa-solid fa-trash"></i></button>
  
      <button onclick="getInfo(${currentProduct.id})" class="btn btn-primary"><i class="fa-solid fa-pen"></i></button>

      
      
      <button type="button" class="btn btn-success" onclick="updateProduct(${currentProduct.id} )"><i class="fa-solid fa-check"></i></button>
      </td>
      </tr>`;
      contentHTML = contentHTML + contentTr;
    }
    document.getElementById("tableProduct").innerHTML = contentHTML;
  }

  function renderUpdateInfo() {
    
  }