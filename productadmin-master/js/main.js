const BASE_URL = "https://633ec06083f50e9ba3b76213.mockapi.io/product";
https://mockapi.io/projects/633ec06083f50e9ba3b76214

var fetchDssvService = function () {
  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then(function (res) {
      renderDsProduct(res.data);
    })
    .catch(function (err) {
      console.log("error: ", err);
    });
};

fetchDssvService();

var deleteProduct = function (id) {

  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchDssvService();
    })
    .catch(function (err) {
      console.log(err);
    });
};

var addProduct = function () {
  var product = layThongTinTuForm();
  var isValid = true;

  isValid = isValid & emptyValidate(product.name, "tbName") && stringValidate(product.name, "tbName");
  isValid = isValid & emptyValidate(product.price, "tbPrice") && priceValidate(product.price, "tbPrice");
  isValid = isValid & emptyValidate(product.screen, "tbScreen") && stringValidate(product.screen, "tbScreen");
  isValid = isValid & emptyValidate(product.backCam, "tbBackCam") && stringValidate(product.backCam, "tbBackCam");
  isValid = isValid & emptyValidate(product.frontCam, "tbFrontCam") && stringValidate(product.frontCam, "tbFrontCam");
  isValid = isValid & emptyValidate(product.img, "tbLink") && imgValidate(product.img, "tbLink");
  isValid = isValid & emptyValidate(product.desc, "tbDesc") && stringValidate(product.desc, "tbDesc");
  isValid = isValid & emptyValidate(product.type, "tbType") && stringValidate(product.type, "tbType");
  if (isValid == true) {
    axios({
      url: `${BASE_URL}`,
      method: "POST",
      data: product,
    })
      .then(function (res) {
        fetchDssvService();
      })
      .catch(function (err) {
        console.log('err: ', err);
        Swal.fire('Thêm thất bại');
      })
  }
}

var getInfo = function (id) {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then(function (res) {
      document.getElementById("name").value = res.data.name;
      document.getElementById("price").value = res.data.price;
      document.getElementById("screen").value = res.data.screen;
      document.getElementById("backCam").value = res.data.backCam;
      document.getElementById("frontCam").value = res.data.frontCam;
      document.getElementById("img").value = res.data.img;
      document.getElementById("desc").value = res.data.desc;
      document.getElementById("type").value = res.data.type;
      var id = JSON.stringify(res.data.id);
      localStorage.setItem("key", id);

    })
    .catch(function (err) {
      console.log("error: ", err);
    });
};


var updateProduct = function (id) {
  var product = layThongTinTuForm();
  var isValid = true;

  isValid = isValid & emptyValidate(product.name, "tbName") && stringValidate(product.name, "tbName");
  isValid = isValid & emptyValidate(product.price, "tbPrice") && priceValidate(product.price, "tbPrice");
  isValid = isValid & emptyValidate(product.screen, "tbScreen") && stringValidate(product.screen, "tbScreen");
  isValid = isValid & emptyValidate(product.backCam, "tbBackCam") && stringValidate(product.backCam, "tbBackCam");
  isValid = isValid & emptyValidate(product.frontCam, "tbFrontCam") && stringValidate(product.frontCam, "tbFrontCam");
  isValid = isValid & emptyValidate(product.img, "tbLink") && imgValidate(product.img, "tbLink");
  isValid = isValid & emptyValidate(product.desc, "tbDesc") && stringValidate(product.desc, "tbDesc");
  isValid = isValid & emptyValidate(product.type, "tbType") && stringValidate(product.type, "tbType");
  if (isValid == true) {
    axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: product,
    })
      .then(function (res) {
        fetchDssvService();
      })
      .catch(function (err) {
        console.log(err);
      });
  }
};

