function Product(name, price, screen, backCam, frontCam, img, desc, type) {
    this.name = name;
    this.price = price;
    this.screen = screen;
    this.backCam = backCam;
    this.frontCam = frontCam;
    this.img = img;
    this.desc = desc;
    this.type = type;
}