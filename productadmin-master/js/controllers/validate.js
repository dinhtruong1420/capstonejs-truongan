function emptyValidate(value, idError) {
    if (value.length == 0) {
        document.getElementById(idError).innerText =
            "Trường này không được để rỗng";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function stringValidate(value, idError) {
    if (value.length < 5 || value.length > 20) {
        document.getElementById(idError).innerHTML =
            "Phải từ 5-20 kí số";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function priceValidate(value, idError) {
    if (value < 5000000 || value > 60000000) {
        document.getElementById(idError).innerText =
            "Giá phải từ 5000000 tới 60000000";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function isImage(url) {
    return /\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(url);
}


function imgValidate(value, idError) {
    if(isImage(value) == false) {
        document.getElementById(idError).innerText = "Link hình ảnh không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}


